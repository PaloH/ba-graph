#include <basic_impl.hpp>

#include <algorithms/cyclic_connectivity.hpp>
#include <util.hpp>

#include <mrozek.hpp>
#include <io.hpp>

#include <cassert>

using namespace ba_graph;
using namespace internal;

void test_colour_vector_to_colour_type() {
  ColourVector v = {1, 1, 1, 1};
  ColourVector res = {0, 0, 0, 0};
  assert(colour_vector_to_colour_type(v) == res);

  v = {1, 1, 2, 2};
  res = {0, 0, 1, 1};
  assert(colour_vector_to_colour_type(v) == res);

  v = {1, 1, 2, 2};
  res = {0, 0, 1, 1};
  assert(colour_vector_to_colour_type(v) == res);
}

void test_get_colouring() {
  ColourVector res = {0};
  assert(get_colouring(0, 1) == res);

  res = {2};
  assert(get_colouring(2, 1) == res);

  res = {0, 1};
  assert(get_colouring(3, 2) == res);

  res = {0, 2};
  assert(get_colouring(6, 2) == res);

  res = {0, 1, 2};
  assert(get_colouring(21, 3) == res);

  res = {1, 1, 2, 2};
  assert(get_colouring(76, 4) == res);
}

void test_colouring_to_decimal() {
  ColourVector input = {0, 0};
  assert(colouring_to_decimal(input) == 0);

  input = {1, 0};
  assert(colouring_to_decimal(input) == 1);

  input = {0, 1};
  assert(colouring_to_decimal(input) == 3);

  input = {0, 2};
  assert(colouring_to_decimal(input) == 6);

  input = {0, 1, 2};
  assert(colouring_to_decimal(input) == 21);

  input = {1, 1, 2, 2};
  assert(colouring_to_decimal(input) == 76);
}

void test_colouring_bit_array_to_colour_set() {
  ColourSet res;
  ColouringBitArray input = ColouringBitArray(power(3, 1), false);
  assert(colouring_bit_array_to_colour_set(input) == res);

  res.insert({0, 0, 0});
  input = ColouringBitArray(power(3, 3), false);
  input.set(ColouringBitArray::Index::to_index(0), true);
  assert(colouring_bit_array_to_colour_set(input) == res);

  res.clear();
  res.insert({0, 1, 2});
  res.insert({1, 0, 2});
  res.insert({0, 0, 0});
  res.insert({0, 1, 1});
  res.insert({2, 2, 2});
  input = ColouringBitArray(power(3, 3), false);
  input.set(ColouringBitArray::Index::to_index(21), true);
  input.set(ColouringBitArray::Index::to_index(19), true);
  input.set(ColouringBitArray::Index::to_index(0), true);
  input.set(ColouringBitArray::Index::to_index(26), true);
  input.set(ColouringBitArray::Index::to_index(12), true);
  assert(colouring_bit_array_to_colour_set(input) == res);
}

void test_colour_set_to_bit_array() {
  ColourSet input;
  input.insert({0, 0, 0});
  ColouringBitArray res = ColouringBitArray(power(3, 3), false);
  res.set(ColouringBitArray::Index::to_index(0), true);
  assert(colour_set_to_bit_array(input) == res);

  input.clear();
  input.insert({0, 0, 1, 1});
  input.insert({0, 0, 0, 0});
  input.insert({2, 2, 2, 2});
  res = ColouringBitArray(power(3, 4), false);
  res.set(ColouringBitArray::Index::to_index(36), true);
  res.set(ColouringBitArray::Index::to_index(0), true);
  res.set(ColouringBitArray::Index::to_index(80), true);
  assert(colour_set_to_bit_array(input) == res);
}

void test_get_terminals() {
  Graph g(createG());
  addMultipleV(g, 4);
  std::vector<Vertex> res;
  for (int i = 1; i < 4; i++) {
    addE(g, Location(0, i));
    res.push_back(g[i].v());
  }
  assert(get_terminals(g) == res);
}

void test_add_cycle_to_terminals() {
  Graph g(createG());
  addMultipleV(g, 4);
  for (int i = 1; i < 4; i++) {
    addE(g, Location(0, i));
  }
  add_cycle_to_terminals(g);

  for (auto &rotation : g) {
    assert(rotation.degree() == 3);
  }
}

void test_remove_cycle_from_terminals() {
  Graph g(createG());
  addMultipleV(g, 4);
  std::vector<Edge> pathDecomposition;
  std::vector<Edge> createdEdges;
  for (int i = 1; i < 4; i++) {
    addE(g, Location(0, i));
    createdEdges.push_back(addE(g, Location(i, i % 3 + 1)).e());
  }
  pathDecomposition.push_back(createdEdges[0]);
  remove_cycle_from_terminals(g, pathDecomposition, createdEdges);

  assert(pathDecomposition.size() == 0);
  for (int i = 1; i < 4; i++) {
    assert(!g.contains(Location(i, i % 3 + 1)));
  }
}

void test_add_multiedges() {
  Graph g(createG());
  addV(g, 0);

  std::vector<Vertex> selectedVertices;
  selectedVertices.push_back(g[0].v());
  add_multiedges(g, selectedVertices);

  assert(g.order() == 2);
  assert(g[0].degree() == 2);
  assert(g[1].degree() == 2);
}

void test_remove_multiedges() {
  Graph g(createG());
  addMultipleV(g, 3);

  addE(g, Location(0, 1));
  addE(g, Location(1, 2));
  addE(g, Location(1, 2));

  remove_multiedges(g);

  assert(g.order() == 2);
  for (auto &r : g) {
    assert(r.degree() == 1);
  }
}

void test_throw_for_small_width_multipoles() {
  Graph g(createG());
  try {
    throw_for_small_width_multipoles(g);
    assert(false);
  } catch (const std::exception &e) {
    assert(true);
  }

  addMultipleV(g, 4);
  for (int i = 1; i < 4; i++) {
    addE(g, Location(0, i));
  }

  try {
    throw_for_small_width_multipoles(g);
    assert(true);
  } catch (const std::exception &e) {
    assert(false);
  }
}

void test_setup() {
  Graph g(createG());
  addMultipleV(g, 4);
  for (int i = 1; i < 4; i++) {
    addE(g, Location(0, i));
  }
  auto orderedEdges = setup(g);
  assert(orderedEdges.size() == 3);
  assert(g.order() == 4);
  assert(g[0].degree() == 3);
  for (int i = 1; i < 4; i++) {
    assert(g[i].degree() == 1);
  }
}

void test_connect_multipoles() {
  Graph g1 = read_graph6_line(":Ea@_hn");
  Graph g2 = read_graph6_line(":Ga@eAM|v");
  std::cout << get_terminals_numbers(g1) << "\n";
  std::cout << get_terminals_numbers(g2) << "\n";
  auto [order1, cba1, t1] = get_canonical_colour_set(g1, ColouringMethod::basic);
  std::cout << order1 << "\n";
  auto [order2, cba2, t2] = get_canonical_colour_set(g2, ColouringMethod::basic);
  std::cout << order2 << "\n";


  // std::cout << connect_multipoles(g1, g2, {{Number(1), Number(2)}, {Number(3), Number(3)}, {Number(2), Number(1)}}) << "\n";
  std::cout << connect_multipoles(g1, g1, {{Number(4), Number(4)}}) << "\n";
}

void test_get_canonical_colour_set_of_composite() {
  std::vector<std::string> graphs6 = {":Ga@iHm~", ":GaHIHi~", ":K`?KhobMdIt", ":I`?KhobMdN"};
  std::vector<std::tuple<std::vector<Number>, ColouringBitArray>> canons;
  std::vector<std::shared_ptr<Graph>> graphs;
  for (std::string graph : graphs6) {
    // Graph g = read_graph6_line(graph);
    std::shared_ptr<Graph> g = std::make_shared<Graph>(read_graph6_line(graph));
    auto result = get_canonical_colour_set(*g, ColouringMethod::basic);
    canons.push_back(std::make_tuple(std::get<0>(result), std::get<1>(result)));
    graphs.push_back(g);
  }
  std::vector<std::map<int, int>> connectionMaps = {{{0,2},{1,1}},{{2,1},{0,3}},{{1,3},{0,0},{2,2}}};
  for (auto map : connectionMaps) {
    for (int i = 0; i < canons.size(); i++) {
      auto [order1, cba1] = canons.at(i);
      for (int j = 0; j < canons.size(); j++) {
        auto [order2, cba2] = canons.at(j);
        auto result = get_canonical_colour_set_of_composite_2(cba1, cba2, map);

        std::map<Number, Number> connectionMap;
        for (auto pair : map) {
          connectionMap.insert({order1.at(pair.first), order2.at(pair.second)});
        }
        Graph composite = connect_multipoles(*graphs.at(i), *graphs.at(j), connectionMap);
        auto [order, cba, time] = get_canonical_colour_set(composite, ColouringMethod::basic);
        assert(result == cba);
      }
    }
  }
}

int main() {
  
  test_colour_vector_to_colour_type();

  test_get_colouring();

  test_colouring_to_decimal();

  test_colouring_bit_array_to_colour_set();

  test_colour_set_to_bit_array();

  test_get_terminals();

  test_add_cycle_to_terminals();

  test_remove_cycle_from_terminals();

  test_add_multiedges();

  test_remove_multiedges();

  test_throw_for_small_width_multipoles();

  test_setup();

  test_connect_multipoles();

  test_get_canonical_colour_set_of_composite();
}
