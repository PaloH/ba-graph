#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>

#include <basic_impl.hpp>
#include <io.hpp>
#include <mrozek.hpp>

using namespace ba_graph;

// Reads from stream and outputs unique colouringSets

int count = 0;

void callback(std::string s, Graph &g, Factory &f,
              std::vector<std::pair<int, ColouringBitArray>> *bitArrays) {
  try {
    auto terminals = g.list(RP::degree(1), RT::n());
    if (terminals.size() < 1)
      return;
    auto cba = get_all_colourings(g);
    if (!cba.all_false()) {
      (*bitArrays).emplace_back(std::pair(count, cba));
    }
  } catch (std::runtime_error &e) {
  }
  count++;
}

// Pipe in all k-poles
// checks all multipoles with each other if they can be joined into a snark
int main() {
  std::vector<std::pair<int, ColouringBitArray>> bitArrays;
  read_graph6_stream<std::vector<std::pair<int, ColouringBitArray>>>(
      std::cin, callback, &bitArrays);

  std::vector<int> terminalIds;
  for (int i = 0;
       i < colouringBitArrayToColourSet(bitArrays[0].second)[0].size(); i++) {
    terminalIds.emplace_back(i);
  }
  for (int i = 0; i < bitArrays.size(); i++) {
    auto &first = bitArrays[i];
    for (int j = i; j < bitArrays.size(); j++) {
      auto &second = bitArrays[j];
      auto res = canCreateSnark(terminalIds, first.second, second.second);
      if (res.second) {
        std::cout
            << "Graphs number " << first.first << " and " << second.first
            << " can create a snark, with first having terminals in order:"
            << std::endl
            << res.first << std::endl;
      }
    }
  }

  return 0;
}
