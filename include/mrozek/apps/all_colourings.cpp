#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>

#include <impl/basic/include.hpp>
#include <io/graph6.hpp>
#include <io/print_nice.hpp>

#include <../../mrozek.hpp>

using namespace ba_graph;

std::set<std::vector<std::vector<int>>> colours[19];

// takes a stream of graph6 graphs and outputs all unique colourings
void callback(std::string s, Graph &g, Factory &f, int *i = 0) {
  auto terminals = g.list(RP::degree(1), RT::n());
  try {
    auto cs = getSemiedgeColourings(g);
    colours[terminals.size()].insert(cs);
  } catch (std::runtime_error &e) {
  }
}

int main() {
  int i = 1;
  read_graph6_stream<int>(std::cin, callback, &i);
  for (auto &v : colours) {
    for (auto &c : v) {
      std::cout << c << std::endl;
    }
  }

  return 0;
}
