#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>

#include <basic_impl.hpp>
#include <io.hpp>
#include <mrozek.hpp>

using namespace ba_graph;

// Reads from stream and outputs unique colouringSets

u_int32_t count = 0;

void callback(std::string s, Graph &g, Factory &f,
              std::set<ColourSet> *colourSets) {
  try {
    auto terminals = g.list(RP::degree(1), RT::n());
    if (terminals.size() < 1)
      return;
    auto res = get_canonical_colour_set_old(g);
    if (res.second.size() > 0) {
      (*colourSets).emplace(res.second);
      count++;
    }
  } catch (std::runtime_error &e) {
  }
}

// Pipe in all k-poles to get all found k-colour-sets
int main() {
  std::set<ColourSet> colourSets;
  read_graph6_stream<std::set<ColourSet>>(std::cin, callback, &colourSets);

  std::cout << "Multipoles: " << count << std::endl;
  std::cout << "Number of unique ColourSets in canonical form: "
            << colourSets.size() << std::endl;
  for (auto &colourSet : colourSets) {
    std::cout << colourSet << std::endl;
  }

  return 0;
}
