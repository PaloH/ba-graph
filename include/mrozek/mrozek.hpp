#ifndef BA_GRAPH_MULTIPOLE_MROZEK_HPP
#define BA_GRAPH_MULTIPOLE_MROZEK_HPP

#include <set>
#include <stdexcept>
#include <vector>
#include <chrono>
#include <algorithm>
#include <numeric>

#include <snarks/colouring_pd.hpp>

#include <algorithms/path_decomposition/shortest_path_heuristic.hpp>
#include <invariants/colouring.hpp>
#include <invariants/connectivity.hpp>
#include <io/print_nice.hpp>
#include <mrozek/colouring_bit_array_canon.hpp>
#include <util/math.hpp>

using ColourVector = std::vector<int>;
using ColourSet = std::set<ColourVector>;
using ColourKind = std::set<ColourSet>;

inline int factorial(int x) { return (x == 1 ? x : x * factorial(x - 1)); }

namespace ba_graph {

  enum class ColouringMethod { PDColorizer, basic};

namespace internal {

// takes a colour vector - returns equivalent lexically minimal colour vector, called colour type
ColourVector colour_vector_to_colour_type(ColourVector colour_vector) {
  int n[3];
  std::fill_n(n, 3, -1);
  for (int &i : colour_vector) {
    for (int j = 0; j < 3; j++) {
      if (n[j] == -1)
        n[j] = i;
      if (n[j] == i) {
        i = j;
        break;
      }
    }
  }
  return colour_vector;
}

// takes colour as an integer (decimal) and length of colouring (to fill zeros)
// returns minimal colouring
// converts 'colour' from decimal to ternary (last digit has highest value)
ColourVector get_colouring(int colour, int length) {
  ColourVector res;
  res.resize(length);
  for (int i = length - 1; i >= 0; i--) {
    int x = colour / (power(3, i));
    res[i] = x;
    colour = colour % (power(3, i));
  }
  return res;
}

// takes colouring, returns in decimal
// converts 'colouring' from ternary (last digit has highest value) to decimal,
int colouring_to_decimal(ColourVector &colouring) {
  uint_fast64_t num = 0;
  for (int i = colouring.size() - 1; i >= 0; i--) {
    num += colouring[i] * power(3, i);
  }
  return num;
}

} // namespace internal

// takes ColouringBitArray, returns a ColourSet
// resulting colourings have no 'color duplicates'
ColourSet colouring_bit_array_to_colour_set(ColouringBitArray &cba) {
  ColourSet res;
  int colour = 0;
  int size = cba.size().to_int64();
  int length = 0;
  while (size > 1) {
    length++;
    size = size / 3;
  }
  for (auto i = ColouringBitArray::Index(0, 0); i < cba.size(); i++) {
    if (cba.get(i)) {
      ColourVector colourVector = internal::get_colouring(colour, length);
      res.insert(colourVector);
    }
    colour++;
  }

  return res;
}

// takes ColourSet and returns an equivalent ColouringBitArray
ColouringBitArray colour_set_to_bit_array(ColourSet &colourSet) {
  if (colourSet.size() < 1)
    throw std::runtime_error("Empty ColourSet");
  auto cba = ColouringBitArray(power(3, (*colourSet.begin()).size()), false);
  for (auto colour_vector : colourSet) {
    uint_fast64_t num = internal::colouring_to_decimal(colour_vector);
    auto i = ColouringBitArray::Index::to_index(num);
    cba.set(i, true);
  }
  return std::move(cba);
}

namespace internal {

// ************************** Helper functions to use PDColorizer ************************** //

// takes multipole, returns and array of Vertices of degree 1,
// which in our case represent dangling edges
std::vector<Vertex> get_terminals(Graph &g) {
  std::vector<Vertex> terminals;
  for (auto &rotation : g) {
    if (rotation.degree() == 1) {
      terminals.push_back(rotation.v());
    }
  }
  return terminals;
}

std::vector<Number> get_terminals_numbers(Graph &g) {
  std::vector<Number> terminals;
  for (auto &rotation : g) {
    if (rotation.degree() == 1) {
      terminals.push_back(rotation.n());
    }
  }
  return terminals;
}

// takes multipole and returns created edges to remove later
// needed for the path decomposition algorithm
std::vector<Edge> add_cycle_to_terminals(Graph &g) {
  auto terminals = get_terminals(g);
  std::vector<Edge> createdEdges;

  auto length = terminals.size();
  createdEdges.emplace_back(
      std::move(addE(g, terminals[0], terminals[length - 1]).e()));
  if (length == 1)
    return createdEdges;
  for (uint i = 0; i < length - 1; i++) {
    createdEdges.emplace_back(
        std::move(addE(g, terminals[i], terminals[i + 1]).e()));
  }

  return createdEdges;
}

// takes multipole, path decomposition and edges created in addCycleToTerminals
// removes the cycle after calling path decomposition algorithm
void remove_cycle_from_terminals(Graph &g, std::vector<Edge> &pathDecomposition,
                              std::vector<Edge> &createdEdges) {
  // delete cycle from multipole
  for (auto &e : createdEdges) {
    deleteE(g, e);
  }
  // delete cycle from ordered edges
  for (auto &e : createdEdges) {
    int i = 0;
    for (auto &p : pathDecomposition) {
      if (p == e) {
        pathDecomposition.erase(pathDecomposition.begin() + i);
        break;
      }
      i++;
    }
  }
}

// takes multipole and vertices (terminals),
// for each vertex v it creates a new vertex and connects to v with 2 edges
// returns created edges
// is needed for colouring algorithm to work
std::vector<Edge> add_multiedges(Graph &g,
                                std::vector<Vertex> selectedVertices) {
  int j = g.order(); // assumes edges go from 0 to order - 1
  std::vector<Edge> res;
  for (auto i : selectedVertices) {
    auto &r = addV(g, j);
    res.push_back(addE(g, r.v(), i).e());
    res.push_back(addE(g, r.v(), i).e());
    j++;
  }
  return res;
}

// takes multipole
// removes vertices of degree 2 (ie. those from addMultiedges)
void remove_multiedges(Graph &g) {
  std::vector<Vertex> vr;
  for (auto &r : g) {
    if (r.degree() == 2) {
      vr.push_back(r.v());
    }
  }
  for (auto &v : vr) {
    deleteV(g, v);
  }
}

// needed to use path decomposition algorithms
std::vector<Edge> setup(Graph &g) {
  // need to add the cycle, because path decomposition right now
  // only works with cubic graphs
  auto createdEdges = internal::add_cycle_to_terminals(g);

  // works only for connected cubic bridgeless graphs
  // if it has a bridge, throw error
  if (has_cut_edge(g)) {
    throw std::runtime_error(
        "Current implementation does not work with bridges.");
  }

  // path decomposition is used to get ordered edges,
  // that contain all vertices and are part of one path
  // this way we can get them in cannonical
  auto pathDecomposition = shortest_path_heuristic(g, g[0][0].e());
  internal::remove_cycle_from_terminals(g, pathDecomposition.ordered_edges,
                                     createdEdges);
  return pathDecomposition.ordered_edges;
}

// ^^^^^^^^^^^^^^^^^^^^^^^^^^ END: Helper functions to use PDColorizer ^^^^^^^^^^^^^^^^^^^^^^^^^^ //

// path_decomposition may not work well with cycles/multiedge
void throw_for_small_width_multipoles(Graph &g) {
  auto terminals = g.list(RP::degree(1), RT::n());
  if (terminals.size() == 0)
    throw std::runtime_error("Graph has no semiedge");
  if (terminals.size() == 1) {
    throw std::runtime_error("1-poles have no colouring");
  }
}

// source: https://stackoverflow.com/a/42454585
template<class _Tnumber, class _Titerator >
bool next_variation
  (
       _Titerator const& _First
     , _Titerator const& _Last
     , _Tnumber const& _Upper
     , _Tnumber const& _Start = 0
     , _Tnumber const& _Step  = 1
  )
  {
   _Titerator _Next = _First;
   while( _Next  != _Last )
    {
      *_Next += _Step;
     if( *_Next < _Upper )
      {
       return true;
      }
     (*_Next) = _Start;
     ++_Next;
    }
   return false;
}

} // namespace internal

ColourSet get_all_colourings_by_PDColorizer(Graph &g, std::vector<Vertex> selectedTerminals) {
  internal::throw_for_small_width_multipoles(g);

  auto edges = internal::setup(g);
  // need to add multiedges, because PDColourizer works better
  // with vertices of order 2 and 3
  auto multiedges = internal::add_multiedges(g, selectedTerminals);
  edges.insert(edges.end(), multiedges.begin(), multiedges.end());
  // end of setup

  PDColorizer pathDecompositionColorizer;
  pathDecompositionColorizer.initialize(g);
  for (auto edge : edges)
    pathDecompositionColorizer.process_state(edge.v1(), edge.v2());
  auto &state = pathDecompositionColorizer.state;
  // TODO: probably possible to just create new CBA and do "CBA += state[i]"
  // and then "return colouringBitArrayToColourSet(CBA)"
  std::vector<ColouringBitArray> allColourings;
  for (unsigned int i = 0; i < state.size(); i++)
    if (!state[i].all_false())
      allColourings.emplace_back(state[i]);

  ColourSet res;
  for (auto &c : allColourings) {
    auto x = colouring_bit_array_to_colour_set(c);
    res.insert(x.begin(), x.end());
  }

  // cleanup
  internal::remove_multiedges(g);
  return res;
}

ColourSet get_all_colourings_basic(Graph &g, std::vector<Number> selectedTerminals) {
  ColourSet result;
  // find terminal edges based on terminal rotation numbers
  std::vector<Edge> terminalEdges;
  for (auto& terminal : selectedTerminals) {
    auto rotation = g.find(terminal);
    #ifdef BA_GRAPH_DEBUG
      assert(rotation->degree() == 1);
    #endif
    terminalEdges.push_back(rotation->begin()->e());
  }

  EdgeLabeling<int> precolouring(-1);
  ColourVector cv(terminalEdges.size());
  std::fill(cv.begin(), cv.end(), 0);

  // find dupicate terminal edges (can happen when isolated edge is present)
  std::vector<std::vector<int>> sameEdgesIndexes;
  for (int i = 0; i < terminalEdges.size(); i++) {
    for (int j = i+1; j < terminalEdges.size(); j++) {
      if (terminalEdges.at(i) == terminalEdges.at(j)) {
        sameEdgesIndexes.push_back({i,j});
      }
    }
  }

  do {
    // do not allow different colouring on free ends of isolated edge
    if (sameEdgesIndexes.size() != 0) {
      for (std::vector<int> sameEdgeIndexes: sameEdgesIndexes) {
        if (cv.at(sameEdgeIndexes.at(0)) != cv.at(sameEdgeIndexes.at(1))) goto next_iteration;
      }
    }
    // set precolouring
    for (int i = 0; i < cv.size(); i++) {
      precolouring.set(terminalEdges.at(i), cv.at(i));
    }

    if (is_edge_colourable_basic(g, 3, precolouring)) {
      ColourVector copy = cv;
      result.insert(copy);
    }
    next_iteration:;
  } while (internal::next_variation(cv.begin(), cv.end(), 3, 0));

  return result;
}

// #################### permutations
namespace internal {

// takes ColouringBitArray and index to premute
// moves edge from index to end
// returns ColouringBitArray
ColouringBitArray next_permutation(ColouringBitArray &cba, int index) {
  auto [a, b, c] = cba.split3(index);
  a.concatenate_to_special(b);
  a.concatenate_to_special(c);
  cba = a;
  return cba;
}

// takes terminalIds and id to permute
// returns next permutation of terminalIds
std::vector<Number> next_permutation(std::vector<Number> &terminalIds, int id) {
  auto x = terminalIds[id];
  terminalIds.erase(terminalIds.begin() + id);
  terminalIds.push_back(x);
  return terminalIds;
}

// takes terminalIds, ColouringBitArray, id of element to permute,
// and terminalPermutations and cbaPermutations to store return values
// calculates all permutations for terminals and ColouringBitArrays
// so that at index i, terminalPermutations[i] would have
// ColouringBitArray cbaPermutations[i]
void permutations_recursive(std::vector<Number> &terminalIds,
                           ColouringBitArray &cba, int id,
                           std::vector<std::vector<Number>> &terminalPermutations,
                           std::vector<ColouringBitArray> &cbaPermutations) {
  if (id == terminalIds.size() - 1) {
    terminalPermutations.emplace_back(terminalIds);
    next_permutation(terminalIds, id);
    cbaPermutations.emplace_back(cba);
    next_permutation(cba, id);
  } else {
    for (int i = 0; i < terminalIds.size() - id; i++) {
      permutations_recursive(terminalIds, cba, id + 1, terminalPermutations,
                            cbaPermutations);
      next_permutation(terminalIds, id);
      next_permutation(cba, id);
    }
  }
}

// takes terminalIds and ColouringBitArray
// returns all pair of all terminalIds and cba permutations,
// so that at index i, terminalPermutations[i] would have
// ColouringBitArray cbaPermutations[i]
std::pair<std::vector<std::vector<Number>>, std::vector<ColouringBitArray>>
permutations(std::vector<Number> terminalIds, ColouringBitArray cba) {
  std::vector<ColouringBitArray> cbaPermutations;
  std::vector<std::vector<Number>> terminalPermutations;
  permutations_recursive(terminalIds, cba, 0, terminalPermutations,
                        cbaPermutations);
  return std::pair(terminalPermutations, cbaPermutations);
}

} // namespace internal

// gets full colour set for fixed order of terminals, colour permutations are included
ColourSet get_full_colour_set(Graph &g, std::vector<Number> terminals, ColouringMethod method = ColouringMethod::PDColorizer) {
  switch (method) {
    case ColouringMethod::PDColorizer: {
      std::vector<Vertex> selectedTerminals;
      for(auto& terminal : terminals) {
        selectedTerminals.push_back(g.find(terminal)->v());
      }

      return get_all_colourings_by_PDColorizer(g, selectedTerminals);
    }

    case ColouringMethod::basic:
      return get_all_colourings_basic(g, terminals);

    default:
      throw std::runtime_error("get_full_colour_set method not recognized");
  }
}

// gets colour set for fixed order of terminals, colour permutations are excluded
ColourSet get_colour_set(Graph &g, std::vector<Number> terminals, ColouringMethod method = ColouringMethod::PDColorizer) {
  ColourSet fullColourSet = get_full_colour_set(g, terminals, method);
  ColourSet colourSet;
  for (auto &colourVector : fullColourSet) {
    auto cs = internal::colour_vector_to_colour_type(colourVector);
    colourSet.insert(cs);
  }
  return colourSet;
}

// gets canonical representation of colour kind, permutations of terminals included, colour permutations excluded
// returns pair 
std::tuple<std::vector<Number>, ColouringBitArray, int64_t> get_canonical_colour_set(ColourSet fullColourSet, std::vector<Number> terminals) {
  if (fullColourSet.empty()) {
    return {terminals, ColouringBitArray(power(3, terminals.size()), false), 0};
  }

  ColouringBitArray cba = colour_set_to_bit_array(fullColourSet);

  auto t1 = std::chrono::high_resolution_clock::now();
  auto permutations = internal::permutations(terminals, cba);

  std::vector<ColouringBitArray> bitArrayVector;
  for (auto &c : permutations.second) {
    bitArrayVector.emplace_back(c);
  }
  auto minCBA = std::min_element(bitArrayVector.begin(), bitArrayVector.end());
  auto it = std::find(permutations.second.begin(), permutations.second.end(), *minCBA);
  int id = it - permutations.second.begin();
  auto t2 = std::chrono::high_resolution_clock::now();

  return {permutations.first[id], *minCBA, std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count()};
}

std::tuple<std::vector<Number>, ColouringBitArray, int64_t> get_canonical_colour_set(Graph &g, std::vector<Number> terminals, ColouringMethod method = ColouringMethod::PDColorizer) {
  #ifdef BA_GRAPH_DEBUG
    for (auto &rotation : g) {
      assert(rotation.degree() == 1 || rotation.degree() == 3);
    }
  #endif
  ColourSet fullColourSet = get_full_colour_set(g, terminals, method);

  return get_canonical_colour_set(fullColourSet, terminals);
}

std::tuple<std::vector<Number>, ColouringBitArray, int64_t> get_canonical_colour_set(Graph &g, ColouringMethod method = ColouringMethod::PDColorizer) {
  #ifdef BA_GRAPH_DEBUG
    for (auto &rotation : g) {
      assert(rotation.degree() == 1 || rotation.degree() == 3);
    }
  #endif
  
  std::vector<Number> terminals = internal::get_terminals_numbers(g);
  return get_canonical_colour_set(g, terminals, method);
}

std::tuple<std::vector<Number>, std::vector<uint_fast8_t>, int64_t> get_canonical_colour_set_lukotka(Graph &g, ColouringMethod method = ColouringMethod::PDColorizer) {
  #ifdef BA_GRAPH_DEBUG
    for (auto &rotation : g) {
      assert(rotation.degree() == 1 || rotation.degree() == 3);
    }
  #endif

  std::vector<Number> terminals = internal::get_terminals_numbers(g);
  ColourSet colourSet = get_full_colour_set(g, terminals, method);

  ColouringBitArray fullCba = colourSet.empty() ? ColouringBitArray(power(3, terminals.size()), false) : colour_set_to_bit_array(colourSet);

  auto t1 = std::chrono::high_resolution_clock::now();
  auto canon = canonize(fullCba, terminals.size());
  auto t2 = std::chrono::high_resolution_clock::now();

  return {terminals, canon, std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count()};
}

/**
 * Function to connect 2 multipoles togheter on specified terminals.
 * 
 * This can be used only to connect 2 graphs on vertices with degree 1.
 * 
 * @param g1 First multipole
 * @param g2 Second multipole
 * @param connectionMap Map mapping Numbers from g1 to Numbers in g2, that we want to connect. Map can not contain same values.
*/
Graph connect_multipoles(Graph& g1, Graph& g2, std::map<Number, Number> connectionMap) {
  #ifdef BA_GRAPH_DEBUG
    for (auto c : connectionMap) {
      auto r1 = g1.find(c.first);
      assert(r1 != g1.end());
      assert(r1->degree() == 1);
      auto r2 = g2.find(c.second);
      assert(r2 != g2.end());
      assert(r2->degree() == 1);
    }
  #endif

  Factory f;
	Graph G(copy_other_factory(g1, f));

  Mapping<Number> m;
  for (const auto &rot : g2)
    m.set(rot.n(), addV(G, f).n());

  for (auto ii : g2.list(RP::all(), IP::primary()))
    addE(G, Location(m.get(ii->n1()), m.get(ii->n2())), f);

  for (std::pair<Number, Number> c : connectionMap) {
    IncidenceIterator incidence1 = G.find(c.first)->begin();
    Rotation& other1 = incidence1->r1().n() == c.first ? incidence1->r2() : incidence1->r1();
    deleteV(G, c.first, f);
    IncidenceIterator incidence2 = G.find(m.get(c.second))->begin();
    Rotation& other2 = incidence2->r1().n() == m.get(c.second) ? incidence2->r2() : incidence2->r1();
    deleteV(G, m.get(c.second), f);
    addE(G, other1, other2, f);
  }

  return G;
}

/**
 * Function to get canonical colour set of graph that would arise from connecting 2 graphs accorfing to connectionMap
 * 
 * IMPORTANT: Both CBA's have to represent full colour sets (colour permutations are included)
 * 
 * @param cba1 ColouringBitArray representing full colour set of the first graph
 * @param cba2 ColouringBitArray representing full colour set of the second graph
 * @param connectionMap Map mapping indexes from one colour vector to other colour vector, that we want to connect. Map can not contain same values.
*/
ColouringBitArray get_canonical_colour_set_of_composite (
  ColouringBitArray cba1,
  ColouringBitArray cba2,
  std::map<int, int> connectionMap
) {
  ColourSet cs1 = colouring_bit_array_to_colour_set(cba1);
  ColourSet cs2 = colouring_bit_array_to_colour_set(cba2);

  ColourSet newColourSet;
  for (ColourVector cv1 : cs1) {
    for (ColourVector cv2 : cs2) {
      bool is_matching = true; // can be connected
      for (auto pair : connectionMap) {
        if (cv1.at(pair.first) != cv2.at(pair.second)) {
          is_matching = false;
          break;
        }
      }

      if (is_matching) {
        ColourVector newColourVector;
        for (int i = 0; i < cv1.size(); i++) {
          if (connectionMap.find(i) == connectionMap.end()) {
            newColourVector.push_back(cv1.at(i));
          }
        }
        for (int i = 0; i < cv2.size(); i++) {
          if (std::find_if(connectionMap.begin(), connectionMap.end(), [i](const auto el) { return el.second == i; }) == connectionMap.end()) {
            newColourVector.push_back(cv2.at(i));
          }
        }
        newColourSet.insert(newColourVector);
      }
    }
  }

  std::vector<Number> newTerminals(cba1.size().get_power() + cba2.size().get_power() - (connectionMap.size() * 2));
  std::iota(std::begin(newTerminals), std::end(newTerminals), 0);
  auto result = get_canonical_colour_set(newColourSet, newTerminals);

  return std::get<1>(result);
}

/**
 * Function to get canonical colour set of graph that would arise from connecting 2 graphs accorfing to connectionMap
 * 
 * IMPORTANT: Both CBA's have to represent full colour sets (colour permutations are included)
 * 
 * @param cba1 ColouringBitArray representing full colour set of the first graph
 * @param cba2 ColouringBitArray representing full colour set of the second graph
 * @param connectionMap Map mapping indexes from one colour vector to other colour vector, that we want to connect. Map can not contain same values.
*/
ColouringBitArray get_canonical_colour_set_of_composite_2 (
  ColouringBitArray cba1,
  ColouringBitArray cba2,
  std::map<int, int> connectionMap
) {
  ColourSet cs1 = colouring_bit_array_to_colour_set(cba1);
  ColourSet cs2 = colouring_bit_array_to_colour_set(cba2);

  ColourSet newColourSet;
  std::vector<int> toBeConnectedIndexes2;
  for (auto pair : connectionMap) {
    toBeConnectedIndexes2.push_back(pair.second);
  }

  std::unordered_map<std::string, std::vector<ColourVector>> hashedCVs;
  for (ColourVector cv : cs1) {
    std::string hash = "";
    ColourVector newCvPart;
    for (int i = 0; i < cv.size(); i++) {
      if (connectionMap.find(i) == connectionMap.end()) {
        newCvPart.push_back(cv.at(i));
      } else {
        hash += std::to_string(cv[i]);
      }
    }

    auto cvI = hashedCVs.find(hash);
    if (cvI == hashedCVs.end()) {
      hashedCVs.insert({hash, {newCvPart}});
    } else {
      cvI->second.push_back(newCvPart);
    }
  }

  for (ColourVector cv : cs2) {
    std::string hash = "";
    for (int i : toBeConnectedIndexes2) {
      hash += std::to_string(cv[i]);
    }
    auto cvI = hashedCVs.find(hash);
    if (cvI != hashedCVs.end()) {
      ColourVector newCvPart2;
      for (int i = 0; i < cv.size(); i++) {
        if (std::find_if(connectionMap.begin(), connectionMap.end(), [i](const auto el) { return el.second == i; }) == connectionMap.end()) {
          newCvPart2.push_back(cv.at(i));
        }
      }
      for (auto cvPart1: cvI->second) {
        cvPart1.insert( cvPart1.end(), newCvPart2.begin(), newCvPart2.end() );
        newColourSet.insert(cvPart1);
      }
    }
  }

  std::vector<Number> newTerminals(cba1.size().get_power() + cba2.size().get_power() - (connectionMap.size() * 2));
  std::iota(std::begin(newTerminals), std::end(newTerminals), 0);
  auto result = get_canonical_colour_set(newColourSet, newTerminals);

  return std::get<1>(result);
}

} // namespace ba_graph
#endif
